#include <linux/module.h>

#include "filter_cdev.h"

int __init init_filter(void) {
	printk(KERN_INFO "filter load\n");

	filter_cdev_init();

	return 0;
}

void __exit exit_filter(void) {
	printk(KERN_INFO "filter unload\n");

	filter_cdev_cleanup();
}

module_init(init_filter);
module_exit(exit_filter);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Wonguk Lee");
MODULE_DESCRIPTION("filter driver for open syscall");
MODULE_VERSION("0.1");
