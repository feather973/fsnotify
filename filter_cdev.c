#include <linux/cdev.h>
#include <linux/sysfs.h>
#include <linux/fs.h>

#define FILTER_DEVICE_NAME		"open_filter"
#define FILTER_MAX_DEVICE		1
#define FILTER_CLASS_NAME		"filter_class"

// TBD : DEVICE_ATTRS

// char dev
int g_filter_major;

struct cdev g_filter_cdev;
struct class *g_filter_class;
struct device *g_filter_device;

// TBD
static int filter_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static int filter_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static long filter_ioctl(struct file *filp, unsigned int, unsigned long)
{
	return 0;
}

static ssize_t filter_read(struct file *filp, char __user *buf, size_t len, loff_t *pos)
{
	return 0;
}

static ssize_t filter_write(struct file *filp, const char __user *buf, size_t len, loff_t *ps)
{
	return 0;
}

static const struct file_operations filter_file_operations = {
	.owner = THIS_MODULE,
	.open = filter_open,
	.release = filter_release,
	.unlocked_ioctl = filter_ioctl,
	.read = filter_read,
	.write = filter_write,
};

int filter_cdev_init(void)
{
	dev_t dev;
	int ret;

	ret = alloc_chrdev_region(&dev, 0, FILTER_MAX_DEVICE, FILTER_DEVICE_NAME);
	if (ret)
		goto out;
	g_filter_major = MAJOR(dev);

	cdev_init(&g_filter_cdev, &filter_file_operations);
	g_filter_cdev.owner = THIS_MODULE;

	ret = cdev_add(&g_filter_cdev, dev, FILTER_MAX_DEVICE);
	if (ret)
		goto unregister_region;

	g_filter_class = class_create(THIS_MODULE, FILTER_CLASS_NAME);
	if (IS_ERR(g_filter_class))
		goto del_cdev;

	g_filter_device = device_create(g_filter_class, NULL, dev, NULL, FILTER_DEVICE_NAME);
	if (IS_ERR(g_filter_device))
		goto unregister_class;

	return 0;	

unregister_class:
	g_filter_device = NULL;

	class_unregister(g_filter_class);
	g_filter_class = NULL;
del_cdev:
	cdev_del(&g_filter_cdev);
unregister_region:
	memset(&g_filter_cdev, 0x0, sizeof(struct cdev));
	unregister_chrdev_region(dev, FILTER_MAX_DEVICE);

	g_filter_major = 0;
out:
	return ret;
}

void filter_cdev_cleanup(void)
{
	if (g_filter_device) {
		device_destroy(g_filter_class, MKDEV(g_filter_major, 0));
		g_filter_device = NULL;
	}

	if (g_filter_class) {
		class_unregister(g_filter_class);
		g_filter_class = NULL;

		cdev_del(&g_filter_cdev);
		memset(&g_filter_cdev, 0x0, sizeof(struct cdev));
	}

	if (g_filter_major) {
		unregister_chrdev_region(MKDEV(g_filter_major, 0), FILTER_MAX_DEVICE);
		g_filter_major = 0;
	}
}

// reference
// https://elixir.bootlin.com/linux/latest/source/drivers/uio/uio.c#L854
// https://elixir.bootlin.com/linux/latest/source/arch/arm/mach-pxa/sharpsl_pm.c#L795
