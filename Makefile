KDIR ?= /lib/modules/`uname -r`/build

default:
	$(MAKE) -C $(KDIR) M=$$PWD

clean:
	rm -f filter.ko *.o *.mod.c Module.symvers modules.order filter.mod
